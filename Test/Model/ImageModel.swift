//
//  ImageModel.swift
//  Test
//
//  Created by Guzlat on 28/3/22.
//

import Foundation
import RealmSwift

class ImageModel: Object, Codable {
    @objc dynamic var id = ""
    @objc dynamic var author = ""
    @objc dynamic var url = ""
    @objc dynamic var download_url = ""
    
    override static func primaryKey() -> String? {
           return "id"
    }
    
    static func create(withName id: String, author: String, url: String, download_url: String ) -> ImageModel {
        let imageData = ImageModel()
        imageData.id = id
        imageData.author = author
        imageData.url = url
        imageData.download_url = download_url
        
        return imageData
    }
}
