//
//  ImagesVC.swift
//  Test
//
//  Created by Guzlat on 28/3/22.
//

import UIKit

class ImagesVC: UIViewController {
    
    private let manager   : ImagesTVManager
    private let viewModel : ImagesVM
    
    private let tableView = UITableView().then {
        $0.backgroundColor = .white
        $0.separatorStyle = .none
        $0.registerdequeueReusableCell(ImageTVCell.self)
    }
    
    init( _ viewModel: ImagesVM){
        self.viewModel = viewModel
        self.manager = .init(viewModel)
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Images"
        
        manager.delegate = self
        tableView.delegate = manager
        tableView.dataSource = manager
        
        let heigth = self.tabBarController?.tabBar.frame.size.height ?? 60
        view.addSubview(tableView)
        tableView.snp.makeConstraints { make in
            make.top.left.right.equalToSuperview()
            make.bottom.equalToSuperview().offset(-heigth)
        }
        
        manager.setup { succes, error in
            if let _ = error {
                print("VM Error: \(String(describing: error))")
                return
            } else {
                self.reloadTV()
                return
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        manager.delegate = self
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        manager.delegate = nil
    }
    
    private func reloadTV(){
        self.tableView.reloadData()
    }
}

extension ImagesVC: ImagesTVManagerDelegate {
    func reloadTVImage() {
        print("reloadTVImages")
        reloadTV()
    }
    
    func didTappOnImage(_ imageData: String) {
        print("vc tapp")
    }
  
}
