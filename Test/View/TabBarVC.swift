//
//  TabBarVC.swift
//  Test
//
//  Created by Guzlat on 28/3/22.
//

import UIKit
import SnapKit

class TabBarVC: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        navigationItem.hidesBackButton = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let imagesVC = UINavigationController(rootViewController: ImagesVC(.init()))
        let imagesItem = UITabBarItem(title: "Images", image: UIImage(named: "home"), selectedImage: UIImage(named: "home"))
        imagesVC.tabBarItem = imagesItem
        
        let randomImageVC = UINavigationController(rootViewController: ImageVC(.init()))
        let randomImageItem = UITabBarItem(title: "Random", image: UIImage(named: "random"), selectedImage: UIImage(named: "random"))
        randomImageVC.tabBarItem = randomImageItem
        
        self.viewControllers = [imagesVC, randomImageVC]
    }
}
