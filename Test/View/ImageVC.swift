//
//  ImageVC.swift
//  Test
//
//  Created by Guzlat on 28/3/22.
//

import UIKit


class ImageVC: UIViewController {
    
    private let viewModel : ImagesVM
    
    lazy var photoIV = UIImageView().then {
        $0.backgroundColor = .clear
        $0.contentMode = .scaleAspectFill
    }
    
    init( _ viewModel: ImagesVM){
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Image"
        
        initUI()
    }
    
    private func initUI(){
        viewModel.getRandomImage { image, error in
            if error != nil {
                self.photoIV.image = UIImage(named: "random")
            } else {
                self.photoIV.image = image
            }
        }
        
        view.addSubview(photoIV)
        photoIV.snp.makeConstraints { make in
            make.center.equalToSuperview()
            make.height.width.equalTo(250.0)
        }
    }
}
