//
//  ImageTVCell.swift
//  Test
//
//  Created by Guzlat on 28/3/22.
//

import UIKit
import Alamofire
import AlamofireImage


class ImageTVCell: UITableViewCell {

    private var photoIV = UIImageView().then {
        $0.backgroundColor = .lightGray
        $0.layer.cornerRadius = 8.0
    }
    
    private var photoLbl = UILabel().then {
        $0.font = UIFont.italicSystemFont(ofSize: 14.0)
        $0.textAlignment = .right
        $0.textColor = .blue
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?){
        
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        contentView.addSubview(photoIV)
        contentView.addSubview(photoLbl)
        photoIV.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(16.0)
            make.left.right.equalToSuperview().inset(24.0)
            make.height.equalTo(200.0)
        }
        photoLbl.snp.makeConstraints { make in
            make.left.right.equalTo(photoIV).inset(4.0)
            make.top.equalTo(photoIV.snp.bottom).offset(8.0)
            make.bottom.equalToSuperview().offset(-8.0)
        }
    }
    
    func setup(_ imageData: ImageModel){
        photoLbl.text = imageData.author
        
        AF.request(imageData.download_url).responseImage { response in
            
            if case .success = response.result {
                if let image = response.value {
                    DispatchQueue.main.async { [self] in
                        photoIV.image = image
                    }
                } else {
                    DispatchQueue.main.async { [self] in
                        photoIV.image = UIImage(named: "random")
                    }
                }
            }
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
