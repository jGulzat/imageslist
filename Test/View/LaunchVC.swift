//
//  ViewController.swift
//  Test
//
//  Created by Guzlat on 28/3/22.
//

import UIKit

class LaunchVC: UIViewController {

    lazy var logoIV = UIImageView().then {
        $0.image = UIImage(named: "logo_optima")
        $0.backgroundColor = .clear
        $0.contentMode = .scaleAspectFill
        $0.alpha = 0
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        view.addSubview(logoIV)
        let logoIVWidth: CGFloat = view.bounds.size.width/3
        
        logoIV.snp.makeConstraints { make in
            make.center.equalToSuperview()
            make.width.equalTo(logoIVWidth)
            make.height.equalTo(logoIVWidth)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        beginAlphaAnimation()
    }
    
    func beginAlphaAnimation() {
        UIView.animate(withDuration: 1, delay: 0.5, options: .curveEaseOut, animations: {
            self.logoIV.alpha = 1
        }) { (isTrue) in
            self.endAlphaAnimation()
        }
    }
    
    fileprivate func endAlphaAnimation() {
        UIView.animate(withDuration: 1, delay: 1, options: .curveEaseIn, animations: {
            self.logoIV.alpha = 0
        }) { (isTrue) in
            self.navigationController?.pushViewController(TabBarVC(), animated: true)
        }
    }

}
