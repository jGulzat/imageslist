//
//  ImagesVM.swift
//  Test
//
//  Created by Guzlat on 28/3/22.
//

import Foundation
import RealmSwift
import UIKit
import Alamofire
import AlamofireImage

final class ImagesVM {

    let service = Service()
    var realmData: Results<ImageModel>!
    let realm = try! Realm()
    
    func getImagesData(_ completion: @escaping (_ data: [ImageModel]?, _ error: Error?) -> ()){
        
        service.requestFetchPhotos { [self] data, error in
                if let _ = error {
                    print("VM Error: \(String(describing: error))")
                    completion(nil, error)
                    return
                } else {
                    guard let imageData = data else { return }
                    
                    realm.beginWrite()
                    realm.add(imageData, update: .modified)
                    try! realm.commitWrite()
                    
                    realmData = realm.objects(ImageModel.self)
                    
                    print("vm succes: \(String(describing: realmData.count))")
                    completion(imageData, nil)
                    return
                }
        }
    }
    
    var imagesCount: Int {
       guard let datas = realmData else { return 0 }
       return datas.count != 0 ? realmData.count : 0
    }
    
    func getImageData(_ at: Int) -> ImageModel {
        realmData[at]
    }
    
    func getImage(_ url: String, _ completion: @escaping () -> ()) -> UIImage{
        var image = UIImage()
        AF.request(url).responseImage { response in
            if response.error != nil {
                image = UIImage(named: "random")!
            } else if case .success = response.result {
                
                if let value = response.value {
                    image = value
                }
            }
        }
        completion()
        return image
    }
    
    func getRandomImage(_ completion: @escaping (_ image: UIImage?, _ error: Error?) -> ()) {

        let count = realm.objects(ImageModel.self).count - 1
        let randomNumber = Int.random(in: 0...count)
        let url = realm.objects(ImageModel.self)[randomNumber].download_url
        
        AF.request(url).responseImage { response in
            
            if let error = response.error {
                completion(nil, error)
            } else if case .success = response.result {
                
                if let image = response.value {
                    completion(image, nil)
                }
            }
        } 
    }

}
