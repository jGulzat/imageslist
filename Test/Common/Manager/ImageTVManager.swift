//
//  ImageTVManager.swift
//  Test
//
//  Created by Guzlat on 28/3/22.
//

import UIKit

protocol ImagesTVManagerDelegate: AnyObject {
    func didTappOnImage(_ imageData: String)
    func reloadTVImage()
}

final class ImagesTVManager: NSObject {
    
    private var viewModel: ImagesVM
    weak var delegate: ImagesTVManagerDelegate?
    
    init(_ viewModel: ImagesVM) {
        self.viewModel = viewModel
    }
    
    func setup(_ completion: @escaping((_ succes: String?, _ error: Error?) -> ())) {
        viewModel.getImagesData({ data, error in
            if let _ = error {
                completion(nil, error)
                return
            } else {
                completion("Succes", nil)
                return
            }

        })
    }
}

extension ImagesTVManager: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.imagesCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let imageDataCell = tableView.dequeueReusableCell(ImageTVCell.self, indexPath: indexPath)
        let data = viewModel.getImageData(indexPath.row)
        /*let url = data.download_url
        let image = viewModel.getImage(url) {
            print("TVManager reloadTVImage")
            self.delegate?.reloadTVImage()
        }*/
        imageDataCell.setup(data) 
        return imageDataCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("didTappImage ManagerTV")
        delegate?.didTappOnImage("hhv")
    }
}
