//
//  UITableView+Extensions.swift
//  Test
//
//  Created by Guzlat on 28/3/22.
//

import UIKit

extension UITableView {
    func registerdequeueReusableCell<T: UITableViewCell>(_ cellType: T.Type) {
        let className = String(describing: cellType)
        let identifier = className + "ID"
        register(cellType, forCellReuseIdentifier: identifier)
    }
    
    func dequeueReusableCell<T: UITableViewCell>(_ cellType: T.Type,
                                                 indexPath: IndexPath) -> T {
        let className = String(describing: cellType)
        let identifier = className + "ID"
        return dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! T
        
    }
}

extension UITableView {
    func scrollToBottom(isAnimated:Bool = true){
        DispatchQueue.main.async { [weak self] in
            guard let self = self, self.numberOfSections > 0 else { return }
            
            let inSection = self.numberOfSections > 0 ? (self.numberOfSections - 1) : 0
            let lastRow   = self.numberOfRows(inSection: inSection) > 0 ? self.numberOfRows(inSection: inSection) - 1 : 0
            let lastSection = self.numberOfSections > 0 ? self.numberOfSections - 1 : 0
            let indexPath = IndexPath(row: lastRow, section: lastSection)
            if self.hasRowAtIndexPath(indexPath: indexPath) {
                self.scrollToRow(at: indexPath, at: .bottom, animated: isAnimated)
            }
        }
    }
    
    func scrollToTop(isAnimated:Bool = true) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            let indexPath = IndexPath(row: 0, section: 0)
            if self.hasRowAtIndexPath(indexPath: indexPath) {
                self.scrollToRow(at: indexPath, at: .top, animated: isAnimated)
            }
        }
    }
    
    func hasRowAtIndexPath(indexPath: IndexPath) -> Bool {
        return indexPath.section < self.numberOfSections && indexPath.row < self.numberOfRows(inSection: indexPath.section)
    }
}
