//
//  Service.swift
//  Test
//
//  Created by Guzlat on 28/3/22.
//

import Foundation
import Alamofire

struct Service {
    
    static let shared = Service()
    
    private var baseUrl = "https://picsum.photos/v2/list"
    
    func requestFetchPhotos(_ completion: @escaping(_ data: [ImageModel]?, _ error: Error?) -> ()){

        AF.request(baseUrl, method: .get).response { response in
            if let _ = response.error {
                completion(nil, response.error)
                return
            } else {
                guard let data = response.data else { return }
                let images = try! JSONDecoder().decode([ImageModel].self, from: data)
                completion(images, nil)
                return
            }
        }
    }
}
